using ConsoleApp;
using ConsoleApp.Repository;
using Microsoft.EntityFrameworkCore;
using Xunit;

namespace XUnitTestProject
{
    public class UnitTest
    {
        [Fact]
        public void PersonManagerTest()
        {
            var options = new DbContextOptionsBuilder<AppDbContext>().UseInMemoryDatabase("UnitTest");

            var manager = new PersonManager(new AppDbContext(options.Options));

            manager.Add(new Person
            {
                Id = 1,
                Name = "Ali",
                Phone = "0918"
            });

            var person = manager.GetFirst();

            Assert.Equal("Ali", person.Name);
        }
    }
}
