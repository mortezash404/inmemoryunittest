﻿using Microsoft.EntityFrameworkCore;

namespace ConsoleApp
{
    public class AppDbContext : DbContext
    {
        public DbSet<Person> Persons { get; set; }

        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
        {

        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
                optionsBuilder.UseSqlServer("server=.;database=UnitTestDb;trusted_connection=true;");

            base.OnConfiguring(optionsBuilder);
        }
    }
}
