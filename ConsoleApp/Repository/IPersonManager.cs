﻿namespace ConsoleApp.Repository
{
    public interface IPersonManager
    {
        void Add(Person person);
        Person GetFirst();
    }
}