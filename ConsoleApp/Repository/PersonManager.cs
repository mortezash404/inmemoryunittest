﻿using System.Linq;

namespace ConsoleApp.Repository
{
    public class PersonManager : IPersonManager
    {
        private readonly AppDbContext _context;

        public PersonManager(AppDbContext context)
        {
            _context = context;
        }

        public void Add(Person person)
        {
            _context.Persons.Add(person);
            _context.SaveChanges();
        }

        public Person GetFirst()
        {
            return _context.Persons.FirstOrDefault();
        }
    }
}
